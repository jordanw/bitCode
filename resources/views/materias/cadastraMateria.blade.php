<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
    <title>Cadastrar notícias</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>

<body>

<section>
<div class="container">

    {!! Form::open(['url' => 'cadastrarMateria']) !!}
       csrf_token()
    <div class="form-group">
        {!! Form::label('Título') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::label('Matéria') !!}
    {!! Form::text('materia', null, ['class' => 'form-control']) !!}

    {!! Form::label('Subtitulo') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}

    {!! Form::label('Descrição') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}

    {!! Form::label('Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}

    {!! Form::label('Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control']) !!}

    {!! Form::submit('Cadastrar') !!}
    {!! Form::close() !!}

    {!!  $data = date('d/m/Y') !!}


</div>
</section>

</body>
</html>